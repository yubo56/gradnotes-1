    \documentclass[11pt,
        usenames, % allows access to some tikz colors
        dvipsnames % more colors: https://en.wikibooks.org/wiki/LaTeX/Colors
    ]{report}
    \usepackage{
        amsmath,
        amssymb,
        fouriernc, % fourier font w/ new century book
        fancyhdr, % page styling
        lastpage, % footer fanciness
        hyperref, % various links
        setspace, % line spacing
        amsthm, % newtheorem and proof environment
        mathtools, % \Aboxed for boxing inside aligns, among others
        float, % Allow [H] figure env alignment
        enumerate, % Allow custom enumerate numbering
        graphicx, % allow includegraphics with more filetypes
        wasysym, % \smiley!
        upgreek, % \upmu for \mum macro
        listings, % writing TrueType fonts and including code prettily
        tikz, % drawing things
        booktabs, % \bottomrule instead of hline apparently
        xcolor, % colored text
        cancel % can cancel things out!
    }
    \usepackage[margin=1in]{geometry} % page geometry
    \usepackage[
        labelfont=bf, % caption names are labeled in bold
        font=scriptsize % smaller font for captions
    ]{caption}
    \usepackage[font=scriptsize]{subcaption} % subfigures

    \newcommand*{\scinot}[2]{#1\times10^{#2}}
    \newcommand*{\dotp}[2]{\left<#1\,\middle|\,#2\right>}
    \newcommand*{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand*{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand*{\rdil}[2]{\mathrm{d}#1 / \mathrm{d}#2}
    \newcommand*{\pdil}[2]{\partial#1 / \partial#2}
    \newcommand*{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand*{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand*{\md}[2]{\frac{\mathrm{D}#1}{\mathrm{D}#2}}
    \newcommand*{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand*{\svec}[1]{\vec{#1}\;\!}
    \newcommand*{\bm}[1]{\boldsymbol{\mathbf{#1}}}
    \newcommand*{\uv}[1]{\hat{\bm{#1}}}
    \newcommand*{\ang}[0]{\;\text{\AA}}
    \newcommand*{\mum}[0]{\;\upmu \mathrm{m}}
    \newcommand*{\at}[1]{\left.#1\right|}
    \newcommand*{\bra}[1]{\left<#1\right|}
    \newcommand*{\ket}[1]{\left|#1\right>}
    \newcommand*{\abs}[1]{\left|#1\right|}
    \newcommand*{\ev}[1]{\left\langle#1\right\rangle}
    \newcommand*{\p}[1]{\left(#1\right)}
    \newcommand*{\s}[1]{\left[#1\right]}
    \newcommand*{\z}[1]{\left\{#1\right\}}

    \newtheorem{theorem}{Theorem}[section]

    \let\Re\undefined
    \let\Im\undefined
    \DeclareMathOperator{\Res}{Res}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Im}{Im}
    \DeclareMathOperator{\Log}{Log}
    \DeclareMathOperator{\Arg}{Arg}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\E}{E}
    \DeclareMathOperator{\Var}{Var}
    \DeclareMathOperator*{\argmin}{argmin}
    \DeclareMathOperator*{\argmax}{argmax}
    \DeclareMathOperator{\sgn}{sgn}
    \DeclareMathOperator{\diag}{diag\;}

    \colorlet{Corr}{red}

    % \everymath{\displaystyle} % biggify limits of inline sums and integrals
    \tikzstyle{circ} % usage: \node[circ, placement] (label) {text};
        = [draw, circle, fill=white, node distance=3cm, minimum height=2em]
    \definecolor{commentgreen}{rgb}{0,0.6,0}
    \lstset{
        basicstyle=\ttfamily\footnotesize,
        frame=single,
        numbers=left,
        showstringspaces=false,
        keywordstyle=\color{blue},
        stringstyle=\color{purple},
        commentstyle=\color{commentgreen},
        morecomment=[l][\color{magenta}]{\#}
    }

\begin{document}

\onehalfspacing

\pagestyle{fancy}
\rfoot{Yubo Su}
\rhead{}
\cfoot{\thepage/\pageref{LastPage}}

\title{MATH 6270---Probability Theory II\\MW1300--1415, Prof.\ Lionel Levine}
\author{Yubo Su}
\date{}

\maketitle

\chapter{02/08/21---Conditional Expectation Part I}

We first define conditional expectation. Given $n$-many $\z{y_i}$ and $\z{z_i}$,
we define
\begin{equation}
    Y = \E\p{X \mid Z} = \sum\limits_{i = 1}^n
        y_i \mathbb{1}_{z = z_i},
\end{equation}
where $y_i = \E\p{X \mid Z = z_i}$. We will start out by building some
intuition about conditional expectation, then we will give more formal
definitions later.

\section{Building Intuition}

\subsection{Easy Case---Discrete Variables}

Consider a probability triple $\p{\Omega, \mathcal{F}_0, P}$ where $\Omega$ is a
set of outcomes, $\mathcal{F}_0$ is a $\sigma$-field of ``events'' (subsets of
$\Omega$), and $P$ is a probability measure that assigns a probability to
everything in $\mathcal{F}_0$, i.e.\ it maps $P: \mathcal{F}_0 \to \s{0, 1}$
(satisfying the axiom of countability).

First, we will consider a random variable $X: \p{\Omega, \mathcal{F}_0} \to
\p{\mathbb{R}, \mathcal{B}}$ where $\mathcal{B}$ is the Borel subsets of the
real numbers. We will assume $X$ is integrable, and that
\begin{equation}
    \E\abs{X} = \int\limits_\Omega \abs{X}\;\mathrm{d}P < \infty.
\end{equation}
We could go ahead and immediately define the conditional expectation in terms of
the $\sigma$-field $\mathcal{F}_0$, but let's first work with some easy cases.

Consider $\E\p{X \mid Z}$ where $X$ and $Z$ are discrete random variables. For
instance, $X \in \z{x_1\dots x_m}$ and $Z \in \z{z_1\dots z_n}$. As we were
probably taught in undergrad, the formula for $X = x_i$ if $Z = z_j$ is given
by:
\begin{equation}
    P\p{X = x_i \mid Z = z_j} := \frac{P\p{X = x_i, Z = z_j}}{P\p{Z = z_j}}.
\end{equation}
This is just Bayes' Law; we can assume the denominator is positive, else the LHS
is just zero. Then, the expectation is
\begin{equation}
    \E\p{X \mid Z = z_j} := \sum\limits_{i = 1}^m x_i P\p{X = x_i \mid Z = z_j}.
\end{equation}
Here, $:=$ is used to mean a definition.

The ``grad school'' version of this is to define $Y = E\p{X \mid Z}$ to be the
random variable where $Y(\omega) = y_j := P\p{X = x_i \mid Z = z_j}$ whenever
$Z(\omega) = z_j$. Intuitively:
\begin{itemize}
    \item We partition $\Omega$ into $n$ many pieces for each $Z = z_j$. We
        could also partition $\Omega$ into $m$ many $X$ pieces, which do not
        necessarily overlap; we don't do that here.

    \item Each partition is assigned $Y = y_j$ as well. $Y$ then is the random
        variable that tells us our best estimate of $X$ given $Z = z_j$. In
        other words, we should think of $Y$ as a random variable as a function
        of $Z$.
\end{itemize}
Again, a simple example, consider the sum of two die rolls. Let $W, Z$ be
independent, uniform on $\s{1, 6}$, and $X = W + Z$. We know that $\E X = \E W +
\E Z = 7$. But what about $\E\p{X \mid Z}$? Recall that this should be a random
variable and should be a function of $Z$. By definition:
\begin{align}
    y_j &= \E\p{w + z \mid z = j},\\
        &= \E W + \E \p{Z \mid Z = j},\\
        &= 3.5 + j,\\
    \E\p{X \mid Z} &= \sum\limits_{j = 1}^6 \p{3.5 + j}\mathbb{1}_{\z{Z = j}},\\
        &= 3.5 + Z.
\end{align}
This is the obvious answer, but we can see that the conditional expectation is
indeed a function of the second variable, $Z$.

\section{Continuous Case and Rigorous Definitions}

As another example, let $U, V$ be independent, uniform on th interval $[0, 1]$.
Let $X = UV$, and let's try to find $\E\p{X \mid U}$. We first heuristically give
the continuous generalizations of the above definitions, and we will give these
rigor later. We have:
\begin{align}
    \E\p{X \mid U} &= \E\p{UV \mid U},\\
        &= U\E \p{V \mid U},\\
        &= U / 2.
\end{align}
We can pull $U$ out of the expectation since it is a constant; we are
\emph{given} $U$. The two pieces of rigor to fill in here are: (i) how is $E\p{X
\mid U}$ defined when $U, V$ are not discrete, and (ii) we need to check the
definition satisfies the [intuitive] manipulations we did above.

\subsection{Conditioning on $\sigma$-Field}
To accomplish the former, we will give the definition where the conditional
variable is a $\sigma$-field, $\E\p{X \mid \mathcal{F}}$. In the discrete case,
\begin{align}
    \mathcal{F} = \sigma\p{Z} &= \z{\z{Z \in B}: B \in \mathcal{B}},\\
        &= \z{\z{Z \in B}: B \subseteq \z{z_1\dots z_n}},\\
        &= \z{\z{Z = z_{i_1}} \cup \dots \cup \z{Z = z_{i_k}}
            : 1 \leq i_1 < \dots < i_k \leq n}.
\end{align}
This is a finite $\sigma$-field, namely $\# \mathcal{F} = 2^n$.

Note that $Y = \E \p{X \mid Z}$ is \emph{constant} on each set $\z{Z = z_j} =
\z{\omega \in \Omega \mid Z(\omega) = z_j}$, so $Y^{-1}\p{y_j} = \z{\z{Z = z_j}}
\in \mathcal{F}$, so $Y \in m\mathcal{F}$ ($\mathcal{F}$-measurable). This is an
important property: if you condition with respect to some $\sigma$-field
$\mathcal{F}$, then it must be measurable with respect to that $\mathcal{F}$.

There is another critical property. Consider the event $A = \z{Z = z_j}$. Then
\begin{align*}
    \int\limits_A X\;\mathrm{d}P
        &= \int\limits_\Omega X \mathbb{1}_A\;\mathrm{d}P,\\
        &= \sum\limits_{i = 1}^m x_i P\p{X \mathbb{1}_{A} = x_i},\\
        &= \sum\limits_{i = 1}^m x_i P\p{X = x_i, A},\\
        &= \sum\limits_{i = 1}^m x_i P\p{X = x_i, Z = z_j},\\
        &= \underbrace{
            \sum\limits_{i = 1}^m x_i P\p{X = x_i \mid Z = z_j}
        }_{y_j} P\p{Z = z_j},\\
        &= P\p{Z = z_j} y_j,\\
        &= \int\limits_A Y\;\mathrm{d}P.
\end{align*}
This is the second property that we desired, so we can now write down the full
definition of conditional probability (properly motivated by two examples),
below.

\subsection{Defining Conditional Expectation}

Given a $\sigma$-field $\mathcal{F} \subseteq \mathcal{F}_0$, the conditional
expectation $\E \p{X \mid \mathcal{F}}$ is any random variable $Y$ satisfying:
\begin{itemize}
    \item $Y \in m\mathcal{F}$, is $\mathcal{F}$-measurable.
    \item $\int\limits_A X\;\mathrm{d}P
        = \int\limits_A Y\;\mathrm{d}P$ $\forall A \in \mathcal{F}$. Can be
        rewritten $\E\p{X \mathbb{1}_A} = \E\p{Y \mathbb{1}_A}$.
\end{itemize}

We said ``any'' above; we need next to prove existence and uniqueness of $Y$.
First, we prove the claim $\E \abs{Y} \leq \E\abs{X}$. Proof: let $A = \z{Y >
0} \in \mathcal{F}$ (since $A = Y^{-1}\p{0, \infty}$), then ($A^c$ is the
complement)
\begin{align}
    \E\p{Y \mathbb{1}_A} &= \E\p{X \mathbb{1}_A},\\
        &= \int\limits_A X\;\mathrm{d}P,\\
        &\leq \int\limits_A \abs{X}\;\mathrm{d}P = \E\p{\abs{X} \mathbb{1}_A},\\
    \E \p{-Y \mathbb{1}_{A^c}} \leq \E\p{\abs{X} \mathbb{1}_{A^c}},\\
    \abs{Y} &= Y\mathbb{1}_A - Y\mathbb{1}_{A^c},\\
    \E \abs{Y} &= \E\p{Y \mathbb{1}_A} - \E\p{Y \mathbb{1}_{A^c}}
        \leq \E\abs{X}\mathbb{1}_A + \E \abs{X}\mathbb{1}_{A^c}.
\end{align}

We first prove uniqueness. Suppose we have two $Y$ and $Y'$ that both satisfy
the two conditions above; we show that the probability that they not equal is
zero. We consider the difference: for $\epsilon > 0$, let the event $A_\epsilon
= \z{Y - Y' \geq \epsilon} \in \mathcal{F}$ (since $Y, Y' \in m\mathcal{F}$, so
too must $Y - Y'$, and thus the event is in $\mathcal{F}$). Then if we integrate
\begin{align}
    \int\limits_{A_\epsilon}\p{Y - Y'}\;\mathrm{d}P &\geq
            \epsilon P(A_\epsilon),\\
        &= \int\limits Y\;\mathrm{d}P - \int\limits Y'\;\mathrm{d}P,\\
        &= \int\limits X\;\mathrm{d}P - \int\limits X\;\mathrm{d}P = 0.
\end{align}
Hence, $P\p{A_\epsilon} = 0$, which shows that their difference is zero. How do
we show that they are equal ``almost surely''? Well, we next want to look at the
event $\z{Y > Y'} = \bigcup_{\epsilon > 0} \z{Y \geq Y' + \epsilon} =
\bigcup_{\epsilon > 0} A_\epsilon$. Then
\begin{align}
    P\p{Y > Y'} &= \lim_{\epsilon \to 0} P\p{A_\epsilon} = 0.
\end{align}
Isn't this union uncountable though, since $\epsilon$ is real? The solution is
to replace the union over $\epsilon$ with the union over any countable
$\epsilon$, e.g.\ $\epsilon = 1/n$. This is because $A_\epsilon \subseteq
A_{\epsilon'}$ if $\epsilon > \epsilon'$. Likewise, $P\p{Y < Y'} = 0$ as well,
so $P\p{Y' \neq Y} = 0$, and thus $P\p{Y = Y'} = 1$, ``$Y = Y'$ almost surely''.
This gives us uniqueness.

\end{document}

