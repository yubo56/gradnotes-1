    \documentclass[11pt,
        usenames, % allows access to some tikz colors
        dvipsnames % more colors: https://en.wikibooks.org/wiki/LaTeX/Colors
    ]{report}
    \usepackage{
        amsmath,
        amssymb,
        fouriernc, % fourier font w/ new century book
        fancyhdr, % page styling
        lastpage, % footer fanciness
        hyperref, % various links
        setspace, % line spacing
        amsthm, % newtheorem and proof environment
        mathtools, % \Aboxed for boxing inside aligns, among others
        float, % Allow [H] figure env alignment
        enumerate, % Allow custom enumerate numbering
        graphicx, % allow includegraphics with more filetypes
        wasysym, % \smiley!
        upgreek, % \upmu for \mum macro
        listings, % writing TrueType fonts and including code prettily
        tikz, % drawing things
        booktabs, % \bottomrule instead of hline apparently
        cancel % can cancel things out!
    }
    \usepackage[margin=1in]{geometry} % page geometry
    \usepackage[
        labelfont=bf, % caption names are labeled in bold
        font=scriptsize % smaller font for captions
    ]{caption}
    \usepackage[font=scriptsize]{subcaption} % subfigures

    \newcommand*{\scinot}[2]{#1\times10^{#2}}
    \newcommand*{\dotp}[2]{\left<#1\,\middle|\,#2\right>}
    \newcommand*{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand*{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand*{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand*{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand*{\md}[2]{\frac{\mathrm{D}#1}{\mathrm{D}#2}}
    \newcommand*{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand*{\svec}[1]{\vec{#1}\;\!}
    \newcommand*{\bm}[1]{\boldsymbol{\mathbf{#1}}}
    \newcommand*{\ang}[0]{\;\text{\AA}}
    \newcommand*{\mum}[0]{\;\upmu \mathrm{m}}
    \newcommand*{\at}[1]{\left.#1\right|}

    \newtheorem{theorem}{Theorem}[section]

    \let\Re\undefined
    \let\Im\undefined
    \DeclareMathOperator{\Res}{Res}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Im}{Im}
    \DeclareMathOperator{\Log}{Log}
    \DeclareMathOperator{\Arg}{Arg}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\E}{E}
    \DeclareMathOperator{\Var}{Var}
    \DeclareMathOperator*{\argmin}{argmin}
    \DeclareMathOperator*{\argmax}{argmax}
    \DeclareMathOperator{\sgn}{sgn}
    \DeclareMathOperator{\diag}{diag\;}

    \DeclarePairedDelimiter\bra{\langle}{\rvert}
    \DeclarePairedDelimiter\ket{\lvert}{\rangle}
    \DeclarePairedDelimiter\abs{\lvert}{\rvert}
    \DeclarePairedDelimiter\ev{\langle}{\rangle}
    \DeclarePairedDelimiter\p{\lparen}{\rparen}
    \DeclarePairedDelimiter\s{\lbrack}{\rbrack}
    \DeclarePairedDelimiter\z{\lbrace}{\rbrace}

    % \everymath{\displaystyle} % biggify limits of inline sums and integrals
    \tikzstyle{circ} % usage: \node[circ, placement] (label) {text};
        = [draw, circle, fill=white, node distance=3cm, minimum height=2em]
    \definecolor{commentgreen}{rgb}{0,0.6,0}
    \lstset{
        basicstyle=\ttfamily\footnotesize,
        frame=single,
        numbers=left,
        showstringspaces=false,
        keywordstyle=\color{blue},
        stringstyle=\color{purple},
        commentstyle=\color{commentgreen},
        morecomment=[l][\color{magenta}]{\#}
    }

\begin{document}

\def\Snospace~{\S{}} % hack to remove the space left after autorefs
\renewcommand*{\sectionautorefname}{\Snospace}
\renewcommand*{\appendixautorefname}{\Snospace}
\renewcommand*{\figureautorefname}{Fig.}
\renewcommand*{\equationautorefname}{Eq.}
\renewcommand*{\tableautorefname}{Tab.}

\onehalfspacing

\pagestyle{fancy}
\rfoot{Yubo Su}
\rhead{}
\cfoot{\thepage/\pageref{LastPage}}

\title{Programming Languages and Logics\\
\small Adrian Sampson MWF 0905--0955}
\author{Yubo Su}

\maketitle

\tableofcontents

\chapter{08/29/18}

I missed the first two classes, catching up now.

\section{Operational Semantics}

Small-step operational semantics is a relation on configurations, consists of
assignments of variables and mutations of variables. When we notate
$\ev*{\sigma, e} \to \ev*{\sigma', e'}$, which means to notate
$\ev*{\ev*{\sigma, e}, \ev*{\sigma', e'}} \in \to$ where $\to$ is the relation
that our rule belongs to. We will define what this $\to$ precisely, with no
ambiguity, later.

As an example, consider $\ev*{\z*{\p*{y, 4}}, x := y + 2; x}$; we have a
store containing the present value of $y$ and we want to assign $x$. Stores are
not allowed to store expressions, they can only store variables (per actual
computer RAM). The evaluation steps (each right arrow step must be a single
step, so stupidly simple) must be
\begin{align*}
    \ev*{\z*{\p*{y, 4}}, x := y + 2; x}
        &\to \ev*{\z*{\p*{y, 4}}, x := 4 + 2; x},\\
        &\to \ev*{\z*{\p*{y, 4}}, x := 6; x},\\
        &\to \ev*{\z*{\p*{y, 4}, \p*{x, 6}}, x},\\
        &\to \ev*{\z*{\p*{y, 4}, \p*{x, 6}}, 6}.
\end{align*}
This is how we expect the program to evaluate such a sequence of instructions;
we haven't specified how the program should execute these steps, but at least we
can agree on what steps such a program should take. A good idea is to develop
one rule for every expression in your grammar.

Let's see some examples of rules to build a language; we notate them
$\frac{\mathrm{premises}}{\mathrm{inference}}\mathrm{rule}$:
\begin{itemize}
    \item Call $\sigma$ the store, then call \lstinline{VAR} (a pure label, no
        semantic value) the operation that interprets $\ev*{\sigma, x} \to
        \ev*{\sigma, n}$ for $\p*{x, n} \in \sigma$. We notate this $\frac{n =
        \sigma(x)}{\ev*{\sigma, x} \to \ev*{\sigma, n}}\mathrm{VAR}$.

    \item Consider the addition expression $\frac{p = m + n}{\ev*{\sigma, n + m}
        \to \ev*{\sigma, p}}\mathrm{ADD}$. Note that the two addition symbols on
        the top and bottom are different pedantically: the top addition is the
        familiar addition symbol (``meta-syntactic'', beyond our syntax), and
        the bottom symbol is part of our grammar that denotes the addition
        relation (``syntactic'', in our syntax).

    \item Consider \lstinline{LADD} defined by
        \begin{equation}
            \frac{\ev*{\sigma, e_1} \to \ev*{\sigma', e_1'}}{
                \ev*{\sigma, e_1 + e_2} \to \ev*{\sigma', e_1' + e_2}}
                \mathrm{LADD}
        \end{equation}
        This is meant to represent left addition of an expression e.g.\ $(1 + 1)
        + 1$. To prove this rule, we \emph{instantiate} it with assignments and
        show that it evaluates correctly with just the premises (and implicit
        axioms) we've put above the line.

    \item \lstinline{RADD} is similar but slightly different:
        \begin{equation}
            \frac{\ev*{\sigma, e_2} \to \ev*{\sigma', e_2'}}{
                \ev*{\sigma, n + e_2} \to \ev*{\sigma', n + e_2'}}
                \mathrm{RADD}
        \end{equation}
        Note that the requirement that $n$ be a number, and not an expression,
        enforces left-to-right addition and eliminates ambiguity.

    \item Similar \lstinline{MUL}, \lstinline{LMUL}, \lstinline{RMUL}.

    \item \lstinline{ASSGN} is defined
        \begin{equation}
            \frac{\sigma' = \sigma[x \mapsto n]}{
                \ev*{\sigma, x := n; e_2} \to \ev*{\sigma', e_2}}\mathrm{ASSGN}.
        \end{equation}
        $\sigma[x \mapsto n]$ (also notated $\sigma\z*{n / x}$) is a
        substitution mapping telling us to read $x$ as $n$.
\end{itemize}
As an example, we call the rule $\ev*{\z*{}, y} \to \ev*{\sigma', e'}$
\emph{stuck}; it cannot be derived, or it does not belong in the relation $\to$.

Some useful program properties that we will prove about our program next
lecture: determinism (every configuration has at most one successor) and
termination (evaluation of every expression terminates). It is tempting to try
to prove soundness (evaluation of every expression yields an integer), but this
is not possible unless we restrict to well-formed configurations $\ev*{\sigma,
e}$ where all variables in $e$ are assigned in $\sigma$. Next lecture!

\chapter{08/31/18}

Let's practice using our operation semantics. Consider evaluating the
configuration $\ev*{\emptyset, \p*{y := 2; 3} + y}$. The second configuration
should perform the assignment, so it goes to $\ev*{\z*{\p*{y, 2}}, 3 + y}$; can
we draw the tree to show that this is a valid transition? We want to find which
rule to apply, which rule our expression is an \emph{instantiation} of. This is
an instantiation of \lstinline{LADD}, so our evaluation tree consists of the
single expression (lol I have janky notation)
\begin{equation*}
    \frac{\z*{\p*{y, 2}} = \emptyset\s*{y \mapsto 2}
        }{\frac{\ev*{\emptyset, y:= 2; 3} \to
        \ev*{\z*{\p*{y, 2}}, 3}}{\ev*{\emptyset, \p*{y := 2; 3} + y} \to
        \ev*{\z*{\p*{y, 2}}}, 3 + y}\mathrm{LADD}}\mathrm{VAR}.
\end{equation*}
The next step is going to be to look up $y$, so
\begin{equation*}
    \frac{\frac{2 = \z*{\p*{y, 2}}(y)}{
        \ev*{\z*{\p*{y, 2}}, 3 + y} \to
        \ev*{\z*{\p*{y, 2}}, 3 + 2}}\mathrm{VAR}}{
        \ev*{\z*{\p*{y, 2}}, 3 + 2} \to \ev*{\z*{\p*{y, 2}}, 5}}\mathrm{LADD}.
\end{equation*}

\section{Program Properties}

We will eventually prove two useful program properties.

\emph{Determinism} is that \emph{every configuration has at most one successor}.
More precisely, $\forall e, e', e'' \in Exp, \forall \sigma, \sigma', \sigma''
\in Store$, if $\ev*{\sigma, e} \to \ev*{\sigma'', e''}$ and $\ev*{\sigma', e'}
\to \ev*{\sigma'', e''}$ then $e = e', \sigma = \sigma'$.

\emph{Termination} is that every expression terminates, I missed the precise
definition.

\emph{Soundness} is that the evaluation of every expression yields an integer.
This is not a property of our program, we have stuck configurations as discussed
last class $\ev*{\emptyset, x}$. But if we restrict ourselves to
\emph{well-formed} configurations $\ev*{\sigma, e}$ where all variables that $e$
refers to have values in $\sigma$, maybe we can prove soundness?

Consider a \emph{free variables} function that takes an expression $e$ and
generates the variables it refers to. Call this $fvs(e)$, then $fvs(x) = \z*{x},
fvs(n) = \z*{}, fvs(e_1 + e_2) = fvs(e_1) \cup fvs(e_2)$, defined inductively.
With all inductively defined expressions, we must watch out that the definitions
terminate and don't infinitely recurse; we can validate this so far. The
remainder of the definition for $fvs$ must be defined on all other operator
combinations, $fvs(e_1 * e_2) = fvs(e_1) \cup fvs(e_2)$ and
$fvs(x:=e_1; e_2) = fvs(e_1) \cup \p*{fvs(e_2) \\ \z*{x}}$.

If we have the list of free variables, we can formulate two properties that
together imply soundness: \emph{progress}, that so long as $\ev*{\sigma, e}$ is
well-formed then we get either an integer or another $\ev*{\sigma', e'}$, and
\emph{preservation}, that well-formedness is preserved.

\subsection{Inductive Sets}

We take yet another detour before proving program properties. An \emph{inductive
set} is a set that is generated by a sequence of inference rules and a base
case, e.g.\ the natural numbers $\frac{}{0 \in \mathbb{N}}, \frac{n \in
\mathbb{N}}{succ(n) \in \mathbb{N}}$ where $succ(n)$ is the successor function.

Note that the multi-step evaluation relation is an inductive set: we have a
bunch of inference rules that define valid configurations. Inductive sets
satisfy properties reminiscent of algebraic ones
\begin{align}
    \frac{}{\ev*{\sigma, e} \to \ev*{\sigma, e}}\mathrm{REFL}&&
    \frac{\ev*{\sigma, e} \to \ev*{\sigma, e}}{
        \ev*{\sigma, e} \to^* \ev*{\sigma, e}} \mathrm{STEP} &&
    \frac{\ev*{\sigma, e} \to \ev*{\sigma', e'},
        \ev*{\sigma', e'} \to \ev*{\sigma'', e''}}{
        \ev*{\sigma, e} \to^* \ev*{\sigma'', e''}}\mathrm{TRANS}.
\end{align}
We notate $\to^*$ to mean ``eventually gets to'', or that a multi-step inference
exists.

One last thing before we go, to prove things about inductive sets we have to
invoke the induction principle. To prove $\forall n, P(n)$ for some principle
$P$, we have to prove base case $P(0)$ and given $P(m < n)$, prove $P(n)$. In
our inference language:
\begin{align}
    \frac{}{a \in A} &&
    \frac{a_1 \in A\dots a_n \in A}{a \in A}.
\end{align}
So we have to assume all the premises above the line to prove for any particular
axiom.

\chapter{09/10/18}

Skipped a few lectures. Last class we started defining a new language that was
somewhat souped up from our simple expression-driven language.

\section{Small-Step Semantics}

The language we were defining has some boolean logic (guessing branching? we
apparently defined \lstinline{seek} last class). The command we were currently
in the middle of discussing is \lstinline{skip}, which is
\begin{equation}
    \frac{\ev*{\sigma, c_1} \to \ev*{\sigma', c_1'}}{
        \ev*{\sigma, c_1; c_2} \to \ev*{\sigma', c_1'; c_2}},
    \frac{}{\ev*{\sigma, skip; c_1} \to \ev*{\sigma; c_1}}.
\end{equation}
To permit branching, we next define \lstinline{if}/\lstinline{then}, as
\begin{align}
    \frac{\ev*{a, b} \to \ev*{\sigma, b'j}}{\ev*{\sigma, \;\mathrm{if} \; b
    \;\mathrm{then}\;c_1 \;\mathrm{else}\; c_2} \to \ev*{\sigma,\; \mathrm{if}\;b
    \;\mathrm{then}\;c_1 \;\mathrm{else}\;c_2}} &&
    \frac{}{\ev*{\sigma, b} \to \ev*{\sigma, c_1}} &&
    \frac{}{\ev*{\sigma, b} \to \ev*{\sigma, c_2}}
\end{align}
Next to define is \lstinline{while}, which is recursively defined
\begin{equation}
    \frac{}{\ev*{\sigma, \;\mathrm{while}\; b \;\mathrm{do}\; c}
        \to \ev*{\sigma, \;\mathrm{if}\; b \;\mathrm{then}\;
            (c; \;\mathrm{while}\; b \;\mathrm{do}\; c)
        \;\mathrm{else}\;\mathrm{skip};}}
\end{equation}

\section{Large-Step Semantics}

For large-step semantics, we again have three relations, one for each syntactic
category. I missed the relations, but they're basically for evaluating numbers,
booleans and for updating the store.

In large-step semantics, we can also write down our other operators,
e.g.\ \lstinline{skip}:
\begin{equation}
    \frac{\mathrm{skip}}{\ev*{\sigma, \mathrm{skip}} \Downarrow \sigma}.
\end{equation}
This should look very uninteresting, and indeed if we started with a large-step
semantic definition of our language, \lstinline{skip} probably wouldn't exist.

We can also write down \lstinline{assign}, which is
\begin{equation}
    \frac{\ev*{\sigma, a} \Downarrow n}{
        \ev*{\sigma, x := \sigma} \Downarrow \sigma \s*{x \mapsto n}}.
\end{equation}
Also \lstinline{seq}
\begin{equation}
    \frac{\ev*{\sigma, c_1} \Downarrow \sigma'\;\;
        \ev*{\sigma', c_2} \Downarrow \sigma''}{
        \ev*{\sigma, c_1; c_2} \Downarrow \sigma''}.
\end{equation}
We can now write down if-then and while
\begin{align*}
    \frac{\ev*{\sigma, b} \Downarrow T \;\; \ev*{\sigma, c_1} \Downarrow \sigma'
        }{\ev*{\sigma, \;\mathrm{if}\; b\;\mathrm{then}\; c_1
        \;\mathrm{else}\;c_2} \Downarrow \sigma'},\\
    \frac{\ev*{\sigma, b} \Downarrow F \;\; \ev*{\sigma, c_2} \Downarrow \sigma'
        }{\ev*{\sigma, \;\mathrm{if}\; b\;\mathrm{then}\; c_1
        \;\mathrm{else}\;c_2} \Downarrow \sigma'},\\
    \frac{\ev*{\sigma, b}\Downarrow F}{
        \ev*{\sigma, \;\mathrm{while}\; b\;\mathrm{do}\;c}\Downarrow \sigma},\\
    \frac{\ev*{\sigma, b} \Downarrow T \;\; \ev*{\sigma, c}\Downarrow \sigma'
        \;\;\ev*{\sigma'; \;\mathrm{while}\; b \;\mathrm{do}\; c}}{
            \ev*{\sigma,\;\mathrm{while}\;b\;\mathrm{do}\; c} \Downarrow
            \sigma}.
\end{align*}
Note that in such recursive definitions, the derivation tree that we write down
must be finite, else our expression is not contained in our defined language;
the program cannot evaluate to anything, rather than it resulting in infinite
evaluations.

Some questions about this language that we've defined, call it IMP\@:
\begin{itemize}
    \item Can you write a program that does not terminate? Yes,
        \lstinline{while true do skip}.

    \item Is IMP Turing complete? It turns out that it's not quite, we have to
        check that the language is not finite state. Since we use the full
        mathematical integers, we do not have finite state.

        To check Turing completeness, we have to show that a TM can run our
        language and our language can run a TM\@.

    \item How much space do we need to represent configurations during execution
        of IMP\@? Infinite space
\end{itemize}

\section{Properties of IMP}

Let's prove determinism, $\forall c \in \mathrm{COM}, \sigma, \sigma', \sigma''
\in \mathrm{STORE}$ then if $\ev*{\sigma, c} \Downarrow \sigma'$ and
$\ev*{\sigma, c} \Downarrow \sigma''$ then $\sigma' = \sigma''$.

To prove this, we could induct over $c$, the set of commands, and examine for
each $c$ in our operational semantics. Here, intsead, we will induct on the
\emph{derivation} of $\ev*{\sigma, c} \Downarrow \sigma'$. The key sentence for
such proofs usually starts $\mathcal{D} \Vdash y$ if the conclusion of
derivation $\mathcal{D}$ is $y$.

The key point on inductions on derivations is that, given a set of axioms and
inference rules, the set of derivations is an inductively defined set. We induct
on the height of a derivation. We generally assume that a property $P$ holds for
a subderivation $\mathcal{D}' \Vdash z$ where $z$ is a subpremise of
$\mathcal{D} \Vdash y$, then $P$ holds for the final $\mathcal{D} \Vdash y$.

\end{document}

