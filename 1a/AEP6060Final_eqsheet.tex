    \documentclass[8pt,
        twocolumn,
        usenames, % allows access to some tikz colors
        dvipsnames % more colors: https://en.wikibooks.org/wiki/LaTeX/Colors
    ]{article}
    \usepackage{
        amsmath,
        amssymb,
        fouriernc, % fourier font w/ new century book
        fancyhdr, % page styling
        lastpage, % footer fanciness
        hyperref, % various links
        setspace, % line spacing
        amsthm, % newtheorem and proof environment
        mathtools, % \Aboxed for boxing inside aligns, among others
        float, % Allow [H] figure env alignment
        enumerate, % Allow custom enumerate numbering
        graphicx, % allow includegraphics with more filetypes
        wasysym, % \smiley!
        upgreek, % \upmu for \mum macro
        listings, % writing TrueType fonts and including code prettily
        tikz, % drawing things
        booktabs, % \bottomrule instead of hline apparently
        cancel, % can cancel things out!
    }
    \usepackage[margin=0.5in, top=0.8in, bottom=0.8in]{geometry} % page geometry
    \usepackage[
        labelfont=bf, % caption names are labeled in bold
        font=scriptsize % smaller font for captions
    ]{caption}
    \usepackage[font=scriptsize]{subcaption} % subfigures

    \newcommand*{\scinot}[2]{#1\times10^{#2}}
    \newcommand*{\bra}[1]{\left<#1\right|}
    \newcommand*{\ket}[1]{\left|#1\right>}
    \newcommand*{\dotp}[2]{\left<#1\,\middle|\,#2\right>}
    \newcommand*{\rd}[2]{\frac{\mathrm{d}#1}{\mathrm{d}#2}}
    \newcommand*{\pd}[2]{\frac{\partial#1}{\partial#2}}
    \newcommand*{\rtd}[2]{\frac{\mathrm{d}^2#1}{\mathrm{d}#2^2}}
    \newcommand*{\ptd}[2]{\frac{\partial^2 #1}{\partial#2^2}}
    \newcommand*{\md}[2]{\frac{\mathrm{D}#1}{\mathrm{D}#2}}
    \newcommand*{\norm}[1]{\left|\left|#1\right|\right|}
    \newcommand*{\abs}[1]{\left|#1\right|}
    \newcommand*{\pvec}[1]{\vec{#1}^{\,\prime}}
    \newcommand*{\svec}[1]{\vec{#1}\;\!}
    \newcommand*{\bm}[1]{\boldsymbol{\mathbf{#1}}}
    \newcommand*{\expvalue}[1]{\left<#1\right>}
    \newcommand*{\ang}[0]{\text{\AA}}
    \newcommand*{\mum}[0]{\upmu \mathrm{m}}
    \newcommand*{\at}[1]{\left.#1\right|}

    \newtheorem{theorem}{Theorem}[section]

    \let\Re\undefined
    \let\Im\undefined
    \DeclareMathOperator{\Res}{Res}
    \DeclareMathOperator{\Re}{Re}
    \DeclareMathOperator{\Im}{Im}
    \DeclareMathOperator{\Log}{Log}
    \DeclareMathOperator{\Arg}{Arg}
    \DeclareMathOperator{\Tr}{Tr}
    \DeclareMathOperator{\E}{E}
    \DeclareMathOperator{\Var}{Var}
    \DeclareMathOperator*{\argmin}{argmin}
    \DeclareMathOperator*{\argmax}{argmax}
    \DeclareMathOperator{\sgn}{sgn}
    \DeclareMathOperator{\diag}{diag\;}

    \DeclarePairedDelimiter\p{\lparen}{\rparen}
    \DeclarePairedDelimiter\s{\lbrack}{\rbrack}
    \DeclarePairedDelimiter\z{\lbrace}{\rbrace}

    % \everymath{\displaystyle} % biggify limits of inline sums and integrals
    \tikzstyle{circ} % usage: \node[circ, placement] (label) {text};
        = [draw, circle, fill=white, node distance=3cm, minimum height=2em]
    \definecolor{commentgreen}{rgb}{0,0.6,0}
    \lstset{
        basicstyle=\ttfamily\footnotesize,
        frame=single,
        numbers=left,
        showstringspaces=false,
        keywordstyle=\color{blue},
        stringstyle=\color{purple},
        commentstyle=\color{commentgreen},
        morecomment=[l][\color{magenta}]{\#}
    }

\begin{document}

\def\Snospace~{\S{}} % hack to remove the space left after autorefs
\renewcommand*{\sectionautorefname}{\Snospace}
\renewcommand*{\appendixautorefname}{\Snospace}
\renewcommand*{\figureautorefname}{Fig.}
\renewcommand*{\equationautorefname}{Eq.}
\renewcommand*{\tableautorefname}{Tab.}

\singlespacing
\scriptsize

\pagestyle{fancy}
\rhead{Yubo Su}

\begin{itemize}
    \item Quantities:
        \begin{align*}
            \lambda_{D\sigma}^2 &=
                \frac{\varepsilon_0 k_BT_\sigma}{q^2n_{0\sigma}},&
            \Omega_\sigma &= \frac{q_\sigma B}{m_\sigma},&
            \omega_{p\sigma}^2
                &= \frac{n_0q_\sigma^2}{m_\sigma \varepsilon_0},\\
            v_{t\sigma}^2 &= \frac{k_BT_\sigma}{m_\sigma},&
            v_A^2 &= \frac{B^2}{\mu_0 \rho},&
            \eta &= \frac{m_e\nu_{ei}}{n_ee^2},\\
            P_B &= \frac{B^2}{2\mu_0},&
            \mu_{conserved} &= \frac{mv_L^2}{2B}.
        \end{align*}

    \item Drifts:
        \begin{description}
            \item[$E \times B$ Drift] $\frac{\vec{E} \times \vec{B}}{B^2}$.
            \item[Polarization] $-\frac{m}{qB^2}\rd{\vec{v}}{t} \times \vec{B}$.
            \item[$\nabla B$ Drift]
                $-\frac{mv^2}{2qB^3}\vec{\nabla}B \times \vec{B}$.
            \item[Curvature] $-\frac{mv_\parallel^2}{R}\hat{R}$ where $R$ is
                curvature vector.
            \item[Dimagnetic (fluid)]
                $-\frac{\vec{\nabla}P_{\sigma 1 \perp} \times \vec{B}_0}{
                    n_0q_\sigma B_0^2}$.
        \end{description}

    \item Fluid Equations:
        \begin{align*}
            \pd{n_\sigma}{t} + \vec{\nabla} \cdot \p*{n_\sigma \vec{u}_\sigma}
                &= 0,\\
            m_\sigma n_\sigma \rd{\vec{u}_\sigma}{t} &=
                n_\sigma q_\sigma \p*{\vec{E} + \vec{u}_\sigma \times \vec{B}}
                    - \vec{\nabla} \cdot \bm{P}_\sigma
                    \s*{- \vec{R}_{\sigma\alpha}}.
        \end{align*}

    \item Vlasov Equation:
        $\rd{f(t, \vec{x}, \vec{v})}{t} = \pd{f_{coll}}{t}$

    \item Maxwell Equations:
        \begin{align*}
            \vec{\nabla} \cdot \vec{E} = -\nabla^2 \Phi
                &= \frac{\rho}{\varepsilon_0},&
            \vec{\nabla} \cdot \vec{B} &= 0,\\
            \vec{\nabla} \times \vec{E} &= -\pd{\vec{B}}{t},&
            \vec{\nabla} \times \vec{B} &= \mu_0 \vec{J}
                + \mu_0 \varepsilon_0 \pd{\vec{E}}{t}.
        \end{align*}

    \item Maxwellian Velocity Distribution:
        \begin{equation*}
            f_\sigma = n_\sigma \p*{\frac{m_\sigma}{2\pi k_BT_\sigma}}^{D/2}
                e^{-\frac{m_\sigma (\vec{v} - \vec{u})^2}{2k_BT_\sigma}}
        \end{equation*}

    \item MHD quantities:
        \begin{align*}
            \vec{J} &= \sum\limits_{\sigma}^{}
                n_\sigma q_\sigma \vec{u}_\sigma,
                &&\text{(Current Density)}\\
            \rho &= \sum\limits_{\sigma}^{} m_\sigma n_\sigma,
                &&\text{(Total Mass Density)}\\
            \vec{U} &= \frac{1}{\rho} \sum\limits_{\sigma}^{}
                m_\sigma n_\sigma \vec{u}_\sigma,
                && \text{(Center-of-mass Velocity)}\\
            \bm{P} &= \sum\limits_{\sigma}^{}
                \int\limits_{}^{}m_\sigma \pvec{v}\pvec{v}
                \;\mathrm{d}^N\pvec{v}.
                && \text{(Total Pressure)}.
        \end{align*}

    \item MHD Equations:
        \begin{align*}
            \pd{\rho}{t} + \vec{\nabla} \cdot (\rho \vec{U}) &= 0,
                &&\text{(Continuity)}\\
            \rho \md{\vec{U}}{t} &=
            \underbrace{
                \left( \sum\limits_{\sigma}^{}n_\sigma q_\sigma \right)
            }_{\approx 0} \vec{E}
                + \vec{J} \times \vec{B} - \vec{\nabla} \cdot \bm{P}
                &&\text{(Momentum)},\\
                \eta\vec{J} &= \vec{E}
                    + \vec{U} \times \vec{B}
                    - \underbrace{
                        \frac{1}{n_ee} \vec{J} \times \vec{B}
                    }_{=0\;\text{(Ideal)}}
                    && \text{(Ohm's Law)}\\
                    &\quad+ \underbrace{\frac{1}{n_ee}\vec{\nabla}(n_ek_BT_e)}
                        _{=0\;(\vec{\nabla} \times)},\\
            \vec{\nabla} \times \vec{E} &= -\pd{\vec{B}}{t},
                && \text{(Faraday's Law)}\\
            \vec{\nabla} \times \vec{B} &= \mu_0 \vec{J}.
                && \text{(Ohm's Law)}
        \end{align*}
    \item General tricks:
        \begin{itemize}
            \item Electrostatic uses Poisson, electromagnetic uses Faraday +
                Amp\`ere.

            \item Really hate life so that the pain of chugging through algebra
                pales in comparison.
        \end{itemize}
    \item No background magnetic field
        \begin{itemize}
            \item Electrostatic ($\vec{B}_0 = \vec{u}_0 = \vec{E}_0 =
                \vec{\nabla}P_0 = 0$ and
                $\vec{\nabla}\Phi_1 \gg -\pd{A_1}{t}$).
                \begin{itemize}
                    \item $\chi_\sigma = -\frac{\omega_{p\sigma}^2}{
                        \omega^2 - \gamma k^2v_{t\sigma}^2}$.

                    \item $\frac{\omega}{k} \gg v_{t\sigma}$---Both species
                        adiabatic, $\omega^2 = \omega_{pe}^2 + \gamma
                        k^2v_{te}^2$, \emph{Langmuir waves}.

                    \item $\frac{\omega}{k} \ll v_{t\sigma}$---Both species
                        isothermal, $\chi_\sigma =
                        \frac{1}{k^2\lambda_{D\sigma}^2}$ Debye shielding,
                        no waves.

                    \item $v_{ti} \ll \frac{\omega}{k} \ll
                        v_{te}$---Adiabatic ions, isothermal electrons,
                        $\omega^2 = \frac{k^2c_s^2}{1 + k^2\lambda_{De}^2}
                        + \gamma k^2v_{ti}^2$ where $c_s^2 =
                        \frac{k_BT_e}{m_i}$ ion acoustic velocity, \emph{ion
                        acoustic waves}.
                \end{itemize}

            \item Electromagnetic ($\vec{B}_1 \neq 0$, relax $\vec{\nabla}
                \Phi_1 \gg -\pd{\vec{A}}{t}$).
                \begin{itemize}
                    \item $\omega^2 = \omega_{pe}^2 + k^2c^2$.
                \end{itemize}

            \item Phase/time delay shifts:
                \begin{itemize}
                    \item $\Delta \phi = \frac{e^2}{2\omega c \mu_e
                        \varepsilon_0} \int\limits_{0}^{L}
                            n_e\;\mathrm{d}x$.
                    \item $\Delta \tau = \frac{c}{\omega} \Delta \phi$.
                \end{itemize}
        \end{itemize}
    \item Alfv\'en Waves ($\vec{B}_0 \neq 0, \omega \ll \omega_{p\sigma},
        \lambda \gg \lambda_{D\sigma}$).
        \begin{itemize}
            \item $B_{1z} = 0, \frac{\omega^2}{k_z^2} \gg v_{t\sigma}^2$
                \emph{Inertial shear/slow Alfv\'en waves}
                \begin{equation*}
                    \omega^2 = \frac{k_z^2v_A^2}{1 +
                        k_{\perp}^2 c^2/\omega_{pe}^2}.
                \end{equation*}

            \item $B_{1z} = 0, \frac{\omega^2}{k^2} \in [v_{ti}, v_{te}]$
                \emph{Kinetic shear Alfv\'en waves}
                \begin{equation*}
                    \omega^2 = k_z^2v_A^2\p*{1 + k_\perp^2\rho_s^2},
                \end{equation*}
                where
                $\rho_s^2 = \frac{\omega_{pi}}{\Omega_i}\lambda_{De}^2$

            \item $B_{1z} = 0, \frac{\omega^2}{k^2} \ll v_{t\sigma}^2$
                Another \emph{kinetic shear Alfv\'en wave}, same as above
                except for $\rho_s$, $\lambda_{De}^2 \to \p*{
                    \lambda_{De}^{-2} + \lambda_{Di}^{-2}
                }^{-1}$

            \item $B_{1z} \neq 0, E_{1z} = J_{1z} = u_{1z} = 0$
                \emph{Compressional/fast Alfv\'en waves} $\frac{n}{B}$ is
                conserved, \emph{frozen-in flow}. Define $c_s^2 = \gamma
                k_B\frac{T_e + T_i}{m_i}$ then
                \begin{equation*}
                    \omega^2 = k^2v_A^2 + k_\perp^2 c_s^2.
                \end{equation*}

            \item MHD treatment gives garbage for shear/slow mode, for fast
                mode gives
                \begin{equation*}
                    \omega^2 = \frac{
                        k^2\p*{v_A^2 + c_s^2} \pm \sqrt{
                            k^4(v_A^2 + c_s^2)^2 - 4k^2k_z^2v_A^2c_s^2}
                        }{2}.
                \end{equation*}
                The mode that in the $k_z = 0$ limit is $\omega^2 =
                k^2\p*{v_A^2 + c_s^2}$ is the magnetosonic mode, the other
                modes are either trivial or nonphysical.
        \end{itemize}
    \item Instabilities
        \begin{itemize}
            \item Cold streaming instability (assume quasineutrality $\vec{B}_1
                = \vec{B}_0 = 0, \vec{u}_0 \neq 0$ and just $\Phi_1$ (HINT HINT
                use Poisson)) gives
                \begin{equation*}
                    \chi_\sigma = -\omega_{p\sigma}^2
                        \frac{1}{\p*{\omega - \vec{k} \cdot \vec{v}_{\sigma0}}^2
                    }.
                \end{equation*}

            \item Kinetic theory treatment (integrate Vlasov equation for plane
                wave perturbation) gives
                \begin{equation*}
                    1 + \frac{q^2}{k^2m\varepsilon_0}
                        \int\limits_{-\infty}^{\infty}
                        \frac{k}{\omega - kv} \pd{f_0}{v}
                        \;\mathrm{d}v = 0.
                \end{equation*}
                Landau Prescription yields ($\alpha =
                \frac{\omega}{kv_{t\sigma}}$)
                \begin{equation*}
                    \begin{split}
                        \chi_\sigma &= \frac{1}{k^2\lambda_{d, \sigma}^2} \s*{
                            1 + \alpha z(\alpha)
                        },\\
                        z(\alpha) &= \frac{1}{\sqrt{\pi}}
                            \int\limits_{-\infty}^{\infty}
                                \frac{e^{-\xi^2}}{\xi - \alpha}
                                \;\mathrm{d}\xi.
                    \end{split}
                \end{equation*}

            \item $\alpha \gg 1$ is Langmuir waves, $\alpha \ll 1$ is ion
                acoustic waves, both damp like
                \begin{equation*}
                    \omega_i = -\sqrt{\frac{\pi}{8}} \frac{\omega_{pe}}{
                        k^3\lambda_{De}^3}e^{-\frac{\omega_r^2}{k^2v_{te}^2}}.
                \end{equation*}

            \item Some intuition is afforded by realizing that a thin
                distribution, corresponding to more fluid like, requires a small
                spread, cold plasma, then damping vanishes.

            \item Penrose criterion says to seek where $Q(z) = k^2(z)$ crosses
                the real axis, and if it is positive then there exist solutions
                (b/c $k^2 > 0$) that are unstable. Boils down to
                \begin{equation*}
                    Q = \frac{q^2}{m\varepsilon_0}\int\limits_{-\infty}^{\infty}
                        \frac{f(v) - f(v_{\min})}{\p*{v - v_{\min}}^2}
                        \;\mathrm{d}v
                \end{equation*}
        \end{itemize}
    \item Cold Plasma Waves
        \begin{itemize}
            \item Boils down to dielectric tensor satisfying $\vec{\nabla}
                \times \vec{B} = -i\omega \mu_0 \bm{K} \cdot \vec{E}$.
                Equivalently $\varepsilon_0 \pd{}{t}\bm{K} \cdot \vec{E} =
                \vec{J} + \varepsilon_0 \pd{\vec{E}}{t}$.

            \item Has form
                \begin{align*}
                    \bm{K} &= \begin{bmatrix}
                        S & -iD & 0\\
                        iD & S & 0\\
                        0 & 0 & P
                    \end{bmatrix},&
                    \begin{aligned}
                        S &= 1 - \sum\limits_{\sigma}^{}
                            \frac{\omega_{p\sigma}^2}{
                            \omega^2 - \Omega_\sigma^2},\\
                        D &= \sum\limits_{\sigma}^{}
                            \frac{\Omega_\sigma}{\omega}
                            \frac{\omega_{p\sigma}^2}
                            {\omega^2 - \Omega_\sigma^2},\\
                        P &= 1 - \sum\limits_{\sigma}^{}
                            \frac{\omega_{p\sigma}^2}{\omega^2}.
                    \end{aligned}
                \end{align*}

            \item Full disperion relation is ($\vec{n} =
                \vec{k}\frac{c}{\omega}$)
                \begin{equation*}
                    \vec{n}\p*{\vec{n} \cdot \vec{E}} - n^2\vec{E}
                        + \bm{K} \cdot \vec{E} =
                    \begin{pmatrix}
                        S - n^2\cos^2\theta & -iD & n^2\sin\theta \cos\theta\\
                        iD & S - n^2 &\\
                        n^2\sin\theta \cos\theta && P - n^2\sin^2\theta
                    \end{pmatrix} \begin{pmatrix}
                        E_x \\ E_y \\ E_z
                    \end{pmatrix} = 0.
                \end{equation*}

            \item Various limits:
                \begin{itemize}
                    \item $\theta = 0$, plasma oscillations $\chi_\sigma =
                        -\frac{\omega_{p\sigma}^2}{\omega^2}$ or left/right
                        polarized modes w/ dispersion relations
                        \begin{equation*}
                            L, R = 0 = 1 - \sum\limits_{\sigma}^{}
                                \frac{\omega_{p\sigma}^2}{\omega\p*{
                                    \omega \mp \Omega_\sigma}}.
                        \end{equation*}
                        In $\omega \to 0$, becomes $1 + \frac{c^2}{v_A^2} = L,
                        R$, Alfv\'en waves $\frac{c^2k_z^2}{\omega^2} =
                        c^2v_A^2$ (missing $1$ term in earlier discussion is
                        displacement current).

                    \item $\theta = \pi/2$, O mode $\omega^2 = k^2c^2 +
                        \sum\limits_{\sigma}^{}\omega_{p\sigma}^2$ and X mode.

                        X mode is $S(S - n^2) - D^2 = 0$ or $n^2 = 2\frac{RL}{R
                        + L} = \frac{S^2 - D^2}{S}$. Resonances at
                        \begin{equation*}
                            S = 0 = 1 - \sum\limits_{\sigma}^{}
                                \frac{\omega_{p\sigma}^2}{\omega^2 -
                                \Omega_\sigma^2}.
                        \end{equation*}
                        $\omega \sim \Omega_e = \omega_{pe}^2 + \Omega_e^2$
                        is the upper hybrid (UH) resonance, $\omega \sim
                        \Omega_i = \Omega_i^2 + \frac{\omega_{pi}^2}{1 +
                        \frac{\omega_{pe}^2}{\Omega_e^2}}$ is the lower hybrid
                        (LH) resonance.

                    \item Low frequency, arbitrary $\theta$, one mode is
                        $\omega^2 = k^2v_A^2$ the fast mode ($(S - n^2)E_y$),
                        other mode is $n_x^2 = \frac{P}{S}\p*{S - n_z^2}$ which
                        solves to
                        \begin{equation*}
                            \omega^2 = \frac{k_z^2v_A^2}{1 +
                            k_x^2c^2/\omega_{pe}^2}.
                        \end{equation*}

                    \item High frequency, arbitrary $\theta$. Can ignore ions,
                        taking $\theta \approx \pi/2$ yields two solutions
                        corresponding to the quasi-transverse ordinary (QTO) and
                        quasi-transverse extraordinary (QTX) modes.

                        Other limit $\theta \approx 0$ is quasi-longitudinal
                        limit, depending on sign gives quasi-longitudinal left
                        (QLL) and quasi-longitudinal right (QLR) waves.
                \end{itemize}
        \end{itemize}

    \item Wave Energy
        \begin{itemize}
            \item Vanilla Poynting Theorem $\pd{w}{t} + \vec{\nabla} \cdot
                \vec{P} = 0$ where
                \begin{equation*}
                    \begin{split}
                        \pd{w}{t} &= \frac{\varepsilon_0 E^2}{2} +
                            \frac{B^2}{2\mu_0} + \vec{J} \cdot \vec{E},\\
                        \vec{P} &= \frac{\vec{E} \times \vec{B}}{\mu_0}.
                    \end{split}
                \end{equation*}
                The third term in $w$ corresponds to particle energy.

            \item In cold plasmas, integrate $w$ to get that the full energy
                \begin{equation*}
                    w(t) - w(-\infty) =
                        \z*{
                            \frac{\varepsilon_0}{4}\tilde{E}^* \cdot \s*{
                                \pd{}{\omega}\p*{\omega \bm{K}}
                            }_{\omega = \omega_r}
                            \cdot \tilde{E} +
                            \frac{1}{4\mu_0}\abs{\tilde{B}}^2
                        }e^{2\omega_i t}.
                \end{equation*}

            \item In warm plasma, modifications to $\vec{P}$ to include a
                $\vec{T}$ flux coming from allowing $\vec{k}_i$, has form
                \begin{equation*}
                    \vec{T} = -\frac{\omega \varepsilon_0}{4}
                        \tilde{E}^* \cdot\s*{
                            \pd{}{\vec{k}} \bm{K}
                        } \cdot \tilde{E}.
                \end{equation*}

            \item Group velocity is $\vec{v}_g = \frac{\vec{P} + \vec{T}}{w}$.
        \end{itemize}

    \item Magnetic Equilibria:
        \begin{itemize}
            \item Magnetic pressure $\frac{B^2}{2\mu_0}$.
            \item Magnetic tension $\p*{\vec{B} \cdot \vec{\nabla}}\vec{B}$.
            \item Magnetic diffusion timescale merges Ohm's and Amp\`ere's Law
                \begin{equation*}
                    \begin{split}
                        \pd{\vec{B}}{t} &=
                            \vec{\nabla} \times \p*{\vec{v} \times \vec{B}}
                                + \frac{1}{\mu_0 \sigma}\nabla^2 \vec{B},\\
                        \vec{B}(t) &=
                            \vec{B}_0e^{-\frac{t}{\mu_0 \sigma L_B^2}}.
                    \end{split}
                \end{equation*}
                $L_B$ is some characteristic lengthscale of magnetic features.
        \end{itemize}
\end{itemize}

\end{document}

